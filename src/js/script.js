$(document).ready(function () {

  $('.hamburger').on('click', function () {
    $('.mobile-menu').addClass('mobile-menu--is-active');
  });

  $('.mobile-menu__close').on('click', function () {
    $('.mobile-menu').removeClass('mobile-menu--is-active');
  });

  var clientsSlider = new Swiper('.clients__slider .swiper-container', {
    autoplay: {
      delay: 5000,
    },
    speed: 750,
    loop: true,
    spaceBetween: 30,
    breakpoints: {
      320: {
        slidesPerView: 1,
        pagination: {
          el: '.clients__slider-pagination',
          type: 'bullets',
        }
      },
      576: {
        slidesPerView: 2,
        pagination: {
          el: '.clients__slider-pagination',
          type: 'bullets',
        }
      },
      768: {
        slidesPerView: 4,
        navigation: {
          nextEl: '.clients__slider-nav-btn--next',
          prevEl: '.clients__slider-nav-btn--prev',
        }
      }
    }
  });


  var closest = require('closest');
  var inputs = document.querySelectorAll( '.field-file__input:not([disabled])' );
  Array.prototype.forEach.call( inputs, function( input ) {
    var label = closest(input, '.field-file').querySelector( '.field-file__name-text' ),
    labelVal = label.innerHTML;
    input.addEventListener( 'change', function( e ) {
      var fileName = '';
      if( this.files && this.files.length > 1 ) {
        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
      }
      else {
        fileName = e.target.value.split( '\\' ).pop();
      }

      if( fileName ) {
        label.innerHTML = fileName;
      }
      else {
        label.innerHTML = labelVal;
      }
    });
  });
});